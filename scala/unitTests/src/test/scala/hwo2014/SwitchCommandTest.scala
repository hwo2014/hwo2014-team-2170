package hwo2014

import org.scalatest.{ShouldMatchers, FlatSpec}
import org.json4s.JsonAST.JString

class SwitchCommandTest extends FlatSpec with ShouldMatchers {
  "toMessage" should "create a Left message if switching left" in {
    SwitchCommand(0, 1, 0, false).toMessage should be(MsgWrapper("switchLane", JString("Left")))
  }

  it should "create a Right message if switching right" in {
    SwitchCommand(0, 0, 1, false).toMessage should be(MsgWrapper("switchLane", JString("Right")))
  }
}
