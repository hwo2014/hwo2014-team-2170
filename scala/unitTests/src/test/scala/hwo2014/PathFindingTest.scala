package hwo2014

import org.scalatest.{ShouldMatchers, FlatSpec}
import PathFinding._

class PathFindingTest extends FlatSpec with ShouldMatchers {
  val defaultStart = PiecePosition(0, 0, LanePosition(0, 0), 0)

  "makeGraph" should "construct a graph correctly from an empty track" in {
    val trackPieces = List(Straight(5.0, false))

    makeGraph(defaultStart, List(Lane(0, 0)), trackPieces) should equal(TrackNode(
      0, 0, Set(Neighbour(Finish, 5.0))
    ))
  }

  it should "construct a graph correctly from a two-node straight track" in {
    val trackPieces = List(Straight(5.0, false), Straight(10.0, false))

    makeGraph(defaultStart, List(Lane(0, 0)), trackPieces) should equal(TrackNode(0, 0, Set(
      Neighbour(
        TrackNode(1, 0, Set(
          Neighbour(Finish, 10.0))),
        5.0
      ))
    ))
  }

  it should "construct a graph correctly from a three-node straight track with a switch in the middle" in {
    val trackPieces = List(
      Straight(3.0, false),
      Straight(5.0, true),
      Straight(8.0, false)
    )

    val lanes = List(
      Lane(-5, 0),
      Lane(5, 1)
    )

    makeGraph(defaultStart, lanes, trackPieces) should equal(TrackNode(0, 0, Set(Neighbour(
      TrackNode(1, 0, Set(
        Neighbour(TrackNode(2, 0, Set(
          Neighbour(Finish, 8.0)
        )), 5.0),
        Neighbour(TrackNode(2, 1, Set(
          Neighbour(Finish, 8.0)
        )), 11.180339887498949)
      )),
      3.0
    ))))
  }

  it should "ignore when the first piece is a switch in terms of constructing paths" in {
    /** i.e., because it doesn't matter, as you're only able to use a switch by issuing a command from the piece
      * previous
      */
    val trackPieces = List(
      Straight(1.0, true),
      Straight(2.0, false)
    )

    makeGraph(defaultStart, List(Lane(0, 0), Lane(10, 1)), trackPieces) should equal(TrackNode(0, 0, Set(Neighbour(
      TrackNode(1, 0, Set(
        Neighbour(Finish, 2.0)
      )),
      1.0
    ))))
  }

  "reconstructPath" should "correctly reconstruct paths" in {
    val piece3 = TrackNode(2, 0, Set(Neighbour(Finish, 3.0)))
    val piece2 = TrackNode(1, 0, Set(Neighbour(piece3, 6.0)))
    val piece1 = TrackNode(0, 0, Set(Neighbour(piece2, 5.0)))

    val cameFrom = Map[Node, Node](
      Finish -> piece3,
      piece3 -> piece2,
      piece2 -> piece1
    )

    reconstructPath(cameFrom, Finish) should equal(List(
      piece1, piece2, piece3, Finish
    ))
  }

  "minimumTraversalHeuristic" should "return the sum distance for only straight pieces" in {
    val trackPieces = List(
      Straight(10.0, false),
      Straight(5.0, true),
      Straight(10.0, false)
    )

    val lanes = List(
      Lane(-5, 0),
      Lane(5, 0)
    )

    minimumTraversalHeuristic(trackPieces, lanes)(0) should equal(25.0)
  }

  it should "return the sum of minimum distances for curved pieces" in {
    val trackPieces = List(
      Bend(10, 90.0),
      Bend(10, -90.0),
      Bend(10, 60.0)
    )

    val lanes = List(
      Lane(-5, 0),
      Lane(0, 1),
      Lane(7, 2)
    )

    minimumTraversalHeuristic(trackPieces, lanes)(0) should equal(
      ((10 - 7) * Math.PI) / 2.0 +
      ((10 - 5) * Math.PI) / 2.0 +
      ((10 - 7) * Math.PI) / 3.0
    )
  }

  "findPath" should "not cross lanes when taking a straight path around a cylinder" in {
    val trackPieces = List(
      Straight(10, true),
      Straight(20, true),
      Straight(10, true)
    )

    val lanes = List(
      Lane(-5, 0),
      Lane(0, 1),
      Lane(5, 2)
    )

    findPath2(defaultStart, lanes, trackPieces).map(_ collect {
      case TrackNode(index, lane, _) => (index, lane)
    }) should equal(Some(List(
      (0, 0),
      (1, 0),
      (2, 0)
    )))
  }

  "findPath" should "cross lanes to be on the inside of a sufficiently large curve" in {
    val trackPieces = List(
      Straight(1.0, false),
      Straight(0.01, true),
      Bend(500, 90, false)
    )

    val lanes = List(
      Lane(0, 0),
      Lane(45, 1)
    )

    findPath2(defaultStart, lanes, trackPieces).map(_ collect {
      case TrackNode(index, lane, _) => (index, lane)
    }) should equal(Some(List(
      (0, 0),
      (1, 0),
      (2, 1)
    )))
  }
}
