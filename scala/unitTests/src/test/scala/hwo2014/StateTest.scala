package hwo2014

import org.scalatest.{OptionValues, ShouldMatchers, FreeSpec}

class StateTest extends FreeSpec with ShouldMatchers with OptionValues with TestData {
  "current position" - {
    val otherCarId = CarId("another", "white")
    val myCurrentPosition = CarPosition(myCarId, 0, PiecePosition(1, 50, LanePosition(1, 1), 1))
    val otherCurrentPosition = CarPosition(otherCarId, 5, PiecePosition(1, 10, LanePosition(1, 1), 1))

    "is None if we don't yet have any car positions" in {
      State().positionOpt should equal (None)
    }

    "is None if we don't yet have our own carId" in {
      State(positions = Map(
        myCarId -> PositionInfo(myCurrentPosition),
        otherCarId -> PositionInfo(otherCurrentPosition)
      )).positionOpt should be(None)
    }

    "is calculated from currentPositions and carId" in {
      State(myCarOpt = Some(myCarId), positions = Map(
        myCarId -> PositionInfo(myCurrentPosition)
      )).positionOpt.value should equal (myCurrentPosition)
    }
  }

  "speed" - {
    val previousGameTick = 0

    "is None if we are missing currentPosition, prevPosition, gameTick, race" in {
      State().speedOpt should equal (None)
    }

    "within the same piece" - {
      val carPosition = CarPosition(myCarId, 0, PiecePosition(0, 15, LanePosition(0, 0), 0))
      val previousPosition = CarPosition(myCarId, 0, PiecePosition(0, 5, LanePosition(0, 0), 0))

      "calculates the correct speed from one tick to the next" in {
        val gameTick = 1
        val state = State(
          gameTickOpt = Some(gameTick),
          myCarOpt = Some(myCarId),
          positions = Map(
            myCarId -> PositionInfo(
              carPosition,
              Some((previousPosition, previousGameTick))
            )
          ),
          raceOpt = Some(Simple.race)
        )
        state.speedOpt.value should equal(10)
      }

      "calculates the correct speed if multiple ticks have passed" in {
        val gameTick = 5
        val state = State(
          gameTickOpt = Some(gameTick),
          myCarOpt = Some(myCarId),
          positions = Map(
            myCarId -> PositionInfo(
              carPosition,
              Some((previousPosition, previousGameTick))
            )
          ),
          raceOpt = Some(Simple.race)
        )
        state.speedOpt.value should equal(2)
      }

      "does not try to calculate the speed if no time has passed" in {
        val gameTick = 0
        val state = State(
          gameTickOpt = Some(gameTick),
          myCarOpt = Some(myCarId),
          positions = Map(
            myCarId -> PositionInfo(
              carPosition,
              Some((previousPosition, previousGameTick))
            )
          ),
          raceOpt = Some(Simple.race)
        )
        state.speedOpt should equal(None)
      }
    }

    "if the car has entered a new piece since the last tick" - {
      val carPosition = CarPosition(myCarId, 0, PiecePosition(1, 5, LanePosition(0, 0), 0))
      val previousPosition = CarPosition(myCarId, 0, PiecePosition(0, 95, LanePosition(0, 0), 0))
      val gameTick = 1

      "calculates the correct speed" in {
        val state = State(
          gameTickOpt = Some(gameTick),
          myCarOpt = Some(myCarId),
          positions = Map(
            myCarId -> PositionInfo(
              carPosition,
              Some((previousPosition, previousGameTick))
            )
          ),
          raceOpt = Some(Simple.race)
        )
        state.speedOpt.value should equal(10)
      }
    }
  }

  "lateralG" - {
    "on a right bend" - {
      val position = CarPosition(myCarId, 0, PiecePosition(1, 10, LanePosition(1, 1), 0))

      "is calculated correctly from speed and current piece" in {
        val state = new State(raceOpt = Some(Simple.race)) {
          override lazy val speedOpt: Option[Double] = Some(20)
          override lazy val positionOpt: Option[CarPosition] = Some(position)
        }
        state.lateralGOpt.value should equal(4)
      }
    }

    "on a left bend" - {
      val position = CarPosition(myCarId, 0, PiecePosition(3, 10, LanePosition(1, 1), 0))

      "is calculated correctly from speed and current piece" in {
        val state = new State(raceOpt = Some(Simple.race)) {
          override lazy val speedOpt: Option[Double] = Some(20)
          override lazy val positionOpt: Option[CarPosition] = Some(position)
        }
        state.lateralGOpt.value should equal(4)
      }
    }

    "on a straight" - {
      val position = CarPosition(myCarId, 0, PiecePosition(0, 10, LanePosition(1, 1), 0))

      "is 0" in {
        val state = new State(raceOpt = Some(Simple.race)) {
          override lazy val speedOpt: Option[Double] = Some(10)
          override lazy val positionOpt: Option[CarPosition] = Some(position)
        }
        state.lateralGOpt.value should equal(0)
      }

      "is None if position is missing" in {
        val state = new State(raceOpt = Some(Simple.race)) {
          override lazy val positionOpt: Option[CarPosition] = None
          override lazy val speedOpt: Option[Double] = Some(10)
        }
        state.lateralGOpt should equal(None)
      }

      "is None if speed is missing" in {
        val state = new State(raceOpt = Some(Simple.race)) {
          override lazy val speedOpt: Option[Double] = None
          override lazy val positionOpt: Option[CarPosition] = Some(position)
        }
        state.lateralGOpt should equal(None)
      }
    }
  }

  "onBend" - {
    "is false if we're on a straight" in {
      val state = new State(raceOpt = Some(Simple.race)) {
        override lazy val positionOpt: Option[CarPosition] = Some(CarPosition(myCarId, 0, PiecePosition(0, 10, LanePosition(0, 0), 0)))
      }
      state.onBendOpt.value should equal(false)
    }

    "is true if we're on a bend" in {
      val state = new State(raceOpt = Some(Simple.race)) {
        override lazy val positionOpt: Option[CarPosition] = Some(CarPosition(myCarId, 0, PiecePosition(1, 10, LanePosition(0, 0), 0)))
      }
      state.onBendOpt.value should equal(true)
    }
  }

  "upcomingPieces" - {
    "on the simple test track" - {
      val tps = Simple.race.track.trackPieces
      "from the first piece will exclude current piece and return the remainder of the track" in {
        val state = new State(raceOpt = Some(Simple.race)) {
          override lazy val positionOpt: Option[CarPosition] = Some(CarPosition(myCarId, 0, PiecePosition(0, 10, LanePosition(0, 0), 0)))
        }
        state.upcomingPiecesOpt.value should equal(List(tps(1), tps(2), tps(3), tps(4)))
      }

      "from the second piece will exclude current piece, return the remainder of the track and append the previous piece on track" in {
        val state = new State(raceOpt = Some(Simple.race)) {
          override lazy val positionOpt: Option[CarPosition] = Some(CarPosition(myCarId, 0, PiecePosition(1, 10, LanePosition(0, 0), 0)))
        }
        state.upcomingPiecesOpt.value should equal(List(tps(2), tps(3), tps(4), tps(0)))
      }

      "from the last piece will exclude current piece and return the rest of the track" in {
        val state = new State(raceOpt = Some(Simple.race)) {
          override lazy val positionOpt: Option[CarPosition] = Some(CarPosition(myCarId, 0, PiecePosition(4, 10, LanePosition(0, 0), 0)))
        }
        state.upcomingPiecesOpt.value should equal(List(tps(0), tps(1), tps(2), tps(3)))
      }
    }

    "handles the edge case gracefully" in {
      val state = new State(raceOpt = Some(EdgeCase.race)) {
        override lazy val positionOpt: Option[CarPosition] = Some(CarPosition(myCarId, 0, PiecePosition(0, 10, LanePosition(0, 0), 0)))
      }
      state.upcomingPiecesOpt.value should equal(Nil)
    }
  }

  "nextBend" - {
    "returns next bend if we're on a straight" in {
      val state = new State(raceOpt = Some(Simple.race)) {
        override lazy val positionOpt: Option[CarPosition] = Some(CarPosition(myCarId, 0, PiecePosition(0, 10, LanePosition(0, 0), 0)))
      }
      state.nextBend.value should equal(Simple.track.trackPieces(1))
    }

    "returns next bend even if it is in the next lap" in {
      val state = new State(raceOpt = Some(Simple.race)) {
        override lazy val positionOpt: Option[CarPosition] = Some(CarPosition(myCarId, 0, PiecePosition(4, 10, LanePosition(0, 0), 0)))
      }
      state.nextBend.value should equal(Simple.track.trackPieces(1))
    }

    "returns the next bend even if we're on a bend now" in {
      val state = new State(raceOpt = Some(Simple.race)) {
        override lazy val positionOpt: Option[CarPosition] = Some(CarPosition(myCarId, 0, PiecePosition(1, 10, LanePosition(0, 0), 0)))
      }
      state.nextBend.value should equal(Simple.track.trackPieces(3))
    }
  }

  "distance to next bend" - {
    "if we'll be on a bend next it should be the remaining length in this piece" in {
      val state = new State(raceOpt = Some(Simple.race)) {
        override lazy val positionOpt: Option[CarPosition] = Some(CarPosition(myCarId, 0, PiecePosition(0, 10, LanePosition(0, 0), 0)))
      }
      state.distanceToNextBend.value should equal(90)
    }

    "gets length to bend across a finish line" in {
      val state = new State(raceOpt = Some(Simple.race)) {
        override lazy val positionOpt: Option[CarPosition] = Some(CarPosition(myCarId, 0, PiecePosition(4, 20, LanePosition(0, 0), 0)))
      }
      state.distanceToNextBend.value should equal(180)
    }

    "works out correct length to bend if there are multiple straight pieces yet to cross" in {
      val state = new State(raceOpt = Some(Real.race)) {
        override lazy val positionOpt: Option[CarPosition] = Some(CarPosition(myCarId, 0, PiecePosition(0, 20, LanePosition(0, 0), 0)))
      }
      state.distanceToNextBend.value should equal(380)
    }

    "gives distance to next bend if we're on a bend" in {
      val state = new State(raceOpt = Some(Simple.race)) {
        override lazy val positionOpt: Option[CarPosition] = Some(CarPosition(myCarId, 0, PiecePosition(1, 10, LanePosition(0, 0), 0)))
      }
      state.distanceToNextBend.value should be > 100.0  // TODO: should do maths and check it exactly but it is late
    }
  }
}
