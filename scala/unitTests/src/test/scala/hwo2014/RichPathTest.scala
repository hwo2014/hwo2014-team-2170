package hwo2014

import SwitchMessageLogic.RichPath
import PathFinding.{TrackNode, Finish}
import org.scalatest.{FlatSpec, ShouldMatchers}

class RichPathTest extends FlatSpec with ShouldMatchers {
  "toSwitchCommand" should "return None if no switches in path" in {
    Seq(Finish).toSwitchCommand should be(None)
  }

  it should "return the appropriate switch command for the first switch in the path" in {
    Seq(
      TrackNode(0, 0, Set()),
      TrackNode(1, 0, Set()),
      TrackNode(2, 1, Set()),
      TrackNode(3, 0, Set()),
      Finish
    ).toSwitchCommand should be(Some(SwitchCommand(1, 0, 1, false)))
  }
}
