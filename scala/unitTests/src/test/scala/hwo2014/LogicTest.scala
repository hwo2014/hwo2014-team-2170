package hwo2014

import org.scalatest.{OptionValues, ShouldMatchers, FreeSpec}

class LogicTest extends FreeSpec with ShouldMatchers with OptionValues with TestData {
  val test2CarId = CarId("test2", "white")

  val myCarPositionFixture = CarPosition(myCarId, 0, PiecePosition(0, 10, LanePosition(0, 0), 0))
  val test2CarPositionFixture = CarPosition(test2CarId, 5, PiecePosition(1, 5, LanePosition(1, 1), 0))

  "updateStateFromPositions" - {
    val newCarPositions = List(
      myCarPositionFixture,
      test2CarPositionFixture
    )

    "sets new car positions" in {
      val state = State()
      val newState = Logic.updateStateFromPositions(state, newCarPositions, 10)
      newState.positions should equal(Map(
        myCarId -> PositionInfo(myCarPositionFixture),
        test2CarId -> PositionInfo(test2CarPositionFixture)
      ))
    }

    "sets game tick" in {
      val state = State()
      val newState = Logic.updateStateFromPositions(state, newCarPositions, 10)
      newState.gameTickOpt.value should equal (10)
    }

    "sets prev position (with tick) to state" in {
      val myOldCarPosition = CarPosition(myCarId, 1, PiecePosition(0, 0, LanePosition(0, 0), 0))

      val state = State(positions = Map(
        myCarId -> PositionInfo(currentPosition = myOldCarPosition, previousPosition = None)
      ), gameTickOpt = Some(0), myCarOpt = Some(myCarId))

      val newState = Logic.updateStateFromPositions(state, newCarPositions, 10)
      newState.previousPositionOpt.value should equal ((myOldCarPosition, 0))
    }

    "unsets last switch command once passing the piece" in {
      val state = State(
        myCarOpt = Some(myCarId),
        lastSwitchCommand = Some(SwitchCommand(
          0, 0, 1, true
        ))
      )
      val newState = Logic.updateStateFromPositions(state, newCarPositions, 10)

      newState.lastSwitchCommand should be(None)
    }
  }

  "update good observed lateralG" - {
    "will get current lateralG and apply it to state as a good value" in {
      val state = new State {
        override lazy val lateralGOpt: Option[Double] = Some(4)
      }
      val newState = Logic.updateGoodLateralG(state)
      newState.mechanics.knownSafeG.value should equal(4)
    }
  }

  "updateStateFromCrash" - {
    val state = new State(mechanics = Mechanics(None, None, 10, 0.02, 50), myCarOpt = Some(myCarId)) {
      override lazy val lateralGOpt: Option[Double] = Some(4)
    }

    "calculates bad lateralG from current state and updates observed lateralG state" in {
      val newState = Logic.updateStateFromCrash(myCarId, state)
      // radius for piece is 100 speed 20 so lateral g calc is same as tests above
      newState.mechanics.knownUnsafeG.value should equal(4)
      newState.mechanics.targetG should equal(4)
    }

    "removes previous position since speed shouldn't be recalculated until the car has respawned" in {
      val newState = Logic.updateStateFromCrash(myCarId, state)
      newState.previousPositionOpt should equal(None)
    }
  }

  "shouldBrake" - {
    "defaults to false!" in {
      Logic.shouldBrake(State()) should equal(false)
    }

    "is true if we're zooming along before a corner" in {
      val position = CarPosition(myCarId, 0, PiecePosition(0, 90, LanePosition(0, 0), 0))
      val state = new State(raceOpt = Some(Simple.race)) {
        override lazy val positionOpt = Some(position)
        override lazy val speedOpt = Some(50.0)
      }
      Logic.shouldBrake(state) should equal(true)
    }

    "is false if we're crawling before a corner" in {
      val position = CarPosition(myCarId, 0, PiecePosition(0, 10, LanePosition(0, 0), 0))
      val state = new State(raceOpt = Some(Simple.race)) {
        override lazy val positionOpt = Some(position)
        override lazy val speedOpt = Some(0.001)
      }
      Logic.shouldBrake(state) should equal(false)
    }

    "should probably tune some careful checks on edges - but later (may change approach)" ignore {}

    "if we're going fast on a gentle corner should still say slow down for upcoming steep corner TODO" ignore {}
  }

  "chooseThrottle" - {
    "should be pedal to the metal if we're slow on a long straight" in {
      val position = CarPosition(myCarId, 0, PiecePosition(0, 1, LanePosition(0, 0), 0))
      val state = new State(raceOpt = Some(Simple.race)) {
        override lazy val positionOpt = Some(position)
        override lazy val speedOpt = Some(0.001)
      }
      Logic.chooseThrottle(state) should equal(1.0)
    }

    "should be braking hard if we're fast approaching a corner" in {
      val position = CarPosition(myCarId, 0, PiecePosition(0, 99, LanePosition(0, 0), 0))
      val state = new State(raceOpt = Some(Simple.race)) {
        override lazy val positionOpt = Some(position)
        override lazy val speedOpt = Some(100.0)
      }
      Logic.chooseThrottle(state) should equal(0)
    }
  }

  "isItTimeForTheTurboButton" - {
    "says no if we don't have a turbo" in {
      val position = CarPosition(myCarId, 0, PiecePosition(0, 99, LanePosition(0, 0), 0))
      val state = new State(raceOpt = Some(Simple.race), availableTurbo = None) {
        override lazy val positionOpt = Some(position)
        override lazy val speedOpt = Some(5.0)
      }
      Logic.isItTimeForTheTurboButton(state) should equal(false)
    }

    "says no on a bend" in {
      val position = CarPosition(myCarId, 0, PiecePosition(1, 99, LanePosition(0, 0), 0))
      val state = new State(raceOpt = Some(Simple.race), availableTurbo = Some(Turbo(100, 30, 3))) {
        override lazy val positionOpt = Some(position)
        override lazy val speedOpt = Some(5.0)
      }
      Logic.isItTimeForTheTurboButton(state) should equal(false)
    }

    "says yes if there are no corners coming up!" in {
      val position = CarPosition(myCarId, 0, PiecePosition(0, 10, LanePosition(0, 0), 0))
      val state = new State(raceOpt = Some(EdgeCase.race), availableTurbo = Some(Turbo(100, 30, 3))) {
        override lazy val positionOpt = Some(position)
        override lazy val speedOpt = Some(5.0)
      }
      Logic.isItTimeForTheTurboButton(state) should equal(true)
    }

    "says yes if we're on a nice long straight" in {
      val position = CarPosition(myCarId, 0, PiecePosition(0, 10, LanePosition(0, 0), 0))
      val state = new State(raceOpt = Some(Real.race), availableTurbo = Some(Turbo(100, 30, 3))) {
        override lazy val positionOpt = Some(position)
        override lazy val speedOpt = Some(5.0)
      }
      Logic.isItTimeForTheTurboButton(state) should equal(true)
    }
  }
}
