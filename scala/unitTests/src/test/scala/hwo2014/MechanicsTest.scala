package hwo2014

import org.scalatest.{OptionValues, ShouldMatchers, FreeSpec}

class MechanicsTest extends FreeSpec with ShouldMatchers with OptionValues {
  "can calculate lateral G-force" in {
    Mechanics.calculateLateralG(100, 20) should equal(4)
  }

  "can calculate target speed for radius and lateral G" in {
    Mechanics.calculateTargetSpeed(16, 4) should equal(8)
  }

  "good lateral G value" - {
    "can be added to empty known safe G" in {
      val initial = Mechanics(None, None, 0, 0.02, 50)
      initial.goodGValue(Some(10)).knownSafeG.value should equal(10)
    }

    "can be added to existing known safe G" in {
      val initial = Mechanics(Some(5), None, 0, 0.02, 50)
      initial.goodGValue(Some(10)).knownSafeG.value should equal(10)
    }

    "does not change lateralG if it is lower than the existing value" in {
      val initial = Mechanics(Some(15), None, 0, 0.02, 50)
      initial.goodGValue(Some(10)).knownSafeG.value should equal(15)
    }

    "when None, doesn't change the LateralG" in {
      val initial = Mechanics(Some(15), None, 0, 0.02, 50)
      initial.goodGValue(None).knownSafeG.value should equal(15)
    }
  }

  "bad lateralG value" - {
    "can be added to empty known unsafe" in {
      val initial = Mechanics(None, None, 0, 0.02, 50)
      initial.badGValue(Some(5)).knownUnsafeG.value should equal(5)
    }

    "can be added to existing known unsafe" in {
      val initial = Mechanics(None, Some(5), 0, 0.02, 50)
      initial.badGValue(Some(10)).knownUnsafeG.value should equal(5)
    }

    "does not change bad lateral g if it is higher than the existing value" in {
      val initial = Mechanics(None, Some(10), 0, 0.02, 50)
      initial.badGValue(Some(15)).knownUnsafeG.value should equal(10)
    }

    "when None, doesn't change the LateralG" in {
      val initial = Mechanics(None, Some(10), 0, 0.02, 50)
      initial.badGValue(None).knownUnsafeG.value should equal(10)
    }

    "reduces target lateralG" in {
      val initial = Mechanics(None, Some(10), 50, 0.02, 50)
      initial.badGValue(Some(10)).targetG should equal(10)
    }

    "does not reduce target lateralG if it is higher than the existing value" in {
      val initial = Mechanics(None, Some(10), 5, 0.02, 50)
      initial.badGValue(Some(10)).targetG should equal(5)
    }
  }

  "distanceToSlow" - {
    "is right for a fast example" in {
      Mechanics(None, None, 0.5, 0.5, 60)
        .distanceToSlow(16, 1) should equal(16.0 + 8.0 + 4.0 + 2.0)
    }

    "is right for a slow example" in {
      Mechanics(None, None, 0.5, 0.5, 60)
        .distanceToSlow(4, 0.25) should equal(4.0 + 2.0 + 1.0 + 0.5)
    }

    "is right for a not-half example" in {
      Mechanics(None, None, 0.5, 0.25, 60)
        .distanceToSlow(16, 1) should equal(16.0 + 4.0)
    }
  }
}
