package hwo2014

import org.json4s.Extraction
import org.json4s.native.Serialization

trait TestData {
  val myCarId = CarId("MyCar", "black")
  val otherCarId = CarId("OtherCar", "white")

  object Real {
    private val raceJson = """{"msgType":"gameInit","data":{"race":{"track":{"id":"keimola","name":"Keimola","pieces":[{"length":100.0},{"length":100.0},{"length":100.0},{"length":100.0,"switch":true},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":200,"angle":22.5,"switch":true},{"length":100.0},{"length":100.0},{"radius":200,"angle":-22.5},{"length":100.0},{"length":100.0,"switch":true},{"radius":100,"angle":-45.0},{"radius":100,"angle":-45.0},{"radius":100,"angle":-45.0},{"radius":100,"angle":-45.0},{"length":100.0,"switch":true},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":200,"angle":22.5},{"radius":200,"angle":-22.5},{"length":100.0,"switch":true},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"length":62.0},{"radius":100,"angle":-45.0,"switch":true},{"radius":100,"angle":-45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"length":100.0,"switch":true},{"length":100.0},{"length":100.0},{"length":100.0},{"length":90.0}],"lanes":[{"distanceFromCenter":-10,"index":0},{"distanceFromCenter":10,"index":1}],"startingPoint":{"position":{"x":-300.0,"y":-44.0},"angle":90.0}},"cars":[{"id":{"name":"MyCar","color":"black"},"dimensions":{"length":40.0,"width":20.0,"guideFlagPosition":10.0}}, {"id":{"name":"OtherCar","color":"white"},"dimensions":{"length":40.0,"width":20.0,"guideFlagPosition":10.0}}],"raceSession":{"laps":3,"maxLapTimeMs":60000,"quickRace":true}}},"gameId":"test-id"}"""
    val race = Serialization.read[MsgWrapper](raceJson).data.extract[GameInit].race
    val cars = race.cars
    val track = race.track
    val pieces = race.track.trackPieces
  }
  object Simple {
    val pieces = List(_TrackPiece(None, None, Some(100), Some(false)), _TrackPiece(Some(105), Some(90), None, None), _TrackPiece(None, None, Some(100), Some(true)), _TrackPiece(Some(95), Some(-90), None, None), _TrackPiece(None, None, Some(100), Some(false)))
    val track = Track("simple", "Simple test track", pieces, List(Lane(-5, 0), Lane(5, 0)), StartingPoint(Position(0, 0), 0))
    val cars = List(Car(myCarId, Dimensions(40, 20, 10)), Car(otherCarId, Dimensions(40, 20, 10)))
    val race = Race(track, cars, RaceSession(Some(3), None, Some(30000), quickRace = Some(true)))
  }
  object EdgeCase {
    val pieces = List(_TrackPiece(None, None, Some(100), Some(false)))
    val track = Track("edgeCase", "Trivial egde case", pieces, List(Lane(-5, 0), Lane(5, 0)), StartingPoint(Position(0, 0), 0))
    val cars = List(Car(myCarId, Dimensions(40, 20, 10)), Car(otherCarId, Dimensions(40, 20, 10)))
    val race = Race(track, cars, RaceSession(Some(3), None, Some(30000), quickRace = Some(true)))
  }
}
