package hwo2014

import org.scalatest.{FlatSpec, ShouldMatchers}

class SwitchMessageLogicTest extends FlatSpec with ShouldMatchers with TestData {
  val stateFixture = State(
    raceOpt = Some(Real.race),
    myCarOpt = Some(myCarId),
    positions = Map(
      myCarId -> PositionInfo(CarPosition(myCarId, 0, PiecePosition(0, 0, LanePosition(0, 0), 1)))
    )
  )

  "switchMessage" should "return a switch message if one stored in the state and we are on the previous tile" in {
    val switchCommand = SwitchCommand(
      1, 0, 1, false
    )

    SwitchMessageLogic.switchMessage(
      stateFixture.copy(lastSwitchCommand = Some(switchCommand))
    ) should be(Some(switchCommand.toMessage))
  }

  it should "not return the switch if the previously sent flag is active" in {
    val switchCommand = SwitchCommand(
      1, 0, 1, true
    )

    SwitchMessageLogic.switchMessage(
      stateFixture.copy(lastSwitchCommand = Some(switchCommand))
    ) should be(None)
  }

  it should "not return the switch if already on the tile" in {
    val switchCommand = SwitchCommand(
      0, 0, 1, true
    )

    SwitchMessageLogic.switchMessage(
      stateFixture.copy(lastSwitchCommand = Some(switchCommand))
    ) should be(None)
  }

  it should "not return the switch if more than one piece previous to the tile" in {
    val switchCommand = SwitchCommand(
      2, 0, 1, true
    )

    SwitchMessageLogic.switchMessage(
      stateFixture.copy(lastSwitchCommand = Some(switchCommand))
    ) should be(None)
  }
}
