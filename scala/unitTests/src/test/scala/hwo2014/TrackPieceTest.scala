package hwo2014

import org.scalatest.{OptionValues, ShouldMatchers}

class TrackPieceTest extends org.scalatest.FreeSpec with ShouldMatchers with OptionValues {
  "deg to radians conversion is correct for a bend piece" in {
    Bend(1, 90).radians should equal (Math.PI / 2)
    Bend(1, -60).radians should equal (-Math.PI / 3)
  }

  "lane length" - {
    "is simple piece length for straight sections" in {
      Straight(10, false).laneLength(Lane(10, 1)) should equal(10)
      Straight(5, false).laneLength(Lane(-5, 2)) should equal(5)
    }

    "is calculated from lane for bends" - {
      "in left turn" - {
        val leftBend = Bend(100, -90)
        "for an outside lane" in {
          val lane = Lane(10, 1)
          leftBend.laneLength(lane) should equal(110 * (Math.PI / 2))
        }
        "for an inside lane" in {
          val lane = Lane(-10, 1)
          leftBend.laneLength(lane) should equal(90 * (Math.PI / 2))
        }
      }
      "in right turn" - {
        val leftBend = Bend(100, 90)
        "for an outside lane" in {
          val lane = Lane(-10, 1)
          leftBend.laneLength(lane) should equal(110 * (Math.PI / 2))
        }
        "for an inside lane" in {
          val lane = Lane(10, 1)
          leftBend.laneLength(lane) should equal(90 * (Math.PI / 2))
        }
      }
    }
  }

  "switchLength" - {
    "is the hypotenuse of the length/lanegap triangle" in {
      val straight = Straight(400, switch = true)
      straight.switchLength(Lane(-200, 1), Lane(100, 2)) should equal(500)
    }
  }

  "pathLength" - {
    "for a bend" - {
      val bends = List(Bend(100, 90, true), Bend(55, 5, true), Bend(200, 360, true), Bend(140, -90, true), Bend(170, -110, true))
      val lanes = List(
        (Lane(-20, 0), Lane(10, 1)),
        (Lane(50, 0), Lane(-10, 1)),
        (Lane(-1, 0), Lane(5, 1))
      )

      "should be more than the inside lane" in {
        for {
          bend <- bends
          (lane1, lane2) <- lanes
          shortLaneLength = Math.min(bend.laneLength(lane1), bend.laneLength(lane2))
        } withClue(s"$bend, $lane1, $lane2") {
          bend.pathLength(lane1, lane2).value should be > shortLaneLength
        }
      }

      "should be less than the outside lane" in {
        for {
          bend <- bends
          (lane1, lane2) <- lanes
          longLaneLength = Math.max(bend.laneLength(lane1), bend.laneLength(lane2))
        } withClue(s"$bend, $lane1, $lane2"){
          bend.pathLength(lane1, lane2).value should be < longLaneLength
        }
      }
    }
  }
}
