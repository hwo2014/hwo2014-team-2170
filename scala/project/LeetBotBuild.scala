import sbt._
import sbtassembly.Plugin._
import AssemblyKeys._
import Keys._
import Dependencies._

object LeetBotBuild extends Build {
  lazy val root = Project("hwo2014bot", file("."))
    .settings(assemblySettings: _*)
    .settings(
      resolvers += "sonatype-public" at "https://oss.sonatype.org/content/groups/public",
      libraryDependencies ++= Seq(
        json4s,
        logback
      ),
      jarName in assembly := "hwo2014bot.jar",
      mainClass in assembly := Some("hwo2014.LeetBot"),
      test in assembly := {}
    )

  lazy val unitTests = Project("unitTests", file("unitTests")).settings(
    libraryDependencies += scalaTest
  ).dependsOn(root)
}
