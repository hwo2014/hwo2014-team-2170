import sbt._

object Dependencies {
  val json4s = "org.json4s" %% "json4s-native" % "3.1.0"

  val logback = "ch.qos.logback" % "logback-classic" % "1.0.9"

  val scalaTest = "org.scalatest" % "scalatest_2.10" % "2.1.3" % "test"
}
