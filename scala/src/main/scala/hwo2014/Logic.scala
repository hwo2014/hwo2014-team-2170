package hwo2014

object Logic {
  def updateStateFromPositions(state: State, carPositions: List[CarPosition], gameTick: Tick): State = {
    val previousPositions = state.gameTickOpt map { tick =>
      state.positions.mapValues { positionInfo =>
        positionInfo.currentPosition -> tick
      }
    }

    val withNewPositions = state.copy(
      positions = (carPositions map { carPosition =>
        carPosition.id -> PositionInfo(
          carPosition,
          previousPositions.flatMap(_.get(carPosition.id))
        )
      }).toMap,
      gameTickOpt = Some(gameTick)
    )

    val shouldClearSwitch = (for {
      currentPos <- withNewPositions.positionOpt
      lastSwitch <- withNewPositions.lastSwitchCommand
    } yield currentPos.piecePosition.pieceIndex == lastSwitch.trackPieceIndex).getOrElse(false)

    if (shouldClearSwitch) {
      withNewPositions.copy(lastSwitchCommand = None)
    } else {
      withNewPositions
    }
  }

  def updateGoodLateralG(state: State): State = {
    val currentLateralGOpt = state.lateralGOpt
    state.copy(
      mechanics = state.mechanics.goodGValue(currentLateralGOpt)
    )
  }

  def updateStateFromCrash(carId: CarId, state: State): State = {
    val currentLateralGOpt = state.lateralGOpt

    val newMechanics = if (state.myCarOpt.exists(_ == carId))
      state.mechanics.badGValue(currentLateralGOpt)
    else
      state.mechanics

    val newPositions = state.positions.get(carId) map { position =>
      state.positions + (carId -> position.copy(previousPosition = None))
    } getOrElse state.positions

    state.copy(
      mechanics = newMechanics,
      positions = newPositions
    )
  }

  def shouldBrake(state: State): Boolean = {
    (for {
      race <- state.raceOpt
      speed <- state.speedOpt
      nextBend <- state.nextBend
      nextCurve <- state.nextCurve
      distanceToNextBend <- state.distanceToNextBend
      position <- state.positionOpt
      currentLane <- position.currentLane(race.track)  // TODO: get lane we will be in, not current
    } yield {
      if (nextCurve.map(_.angle.abs).sum > 30) {
        val targetSpeedForNextCorner = Mechanics.calculateTargetSpeed(nextBend.laneRadius(currentLane), state.mechanics.targetG)
        if (speed > targetSpeedForNextCorner) {
          val distanceRequiredToSlowToTargetSpeed = state.mechanics.distanceToSlow(speed, targetSpeedForNextCorner)
          // if we're within a tick of needing to brake, we'd best do so
          if (speed + distanceRequiredToSlowToTargetSpeed - distanceToNextBend > 0) true
          else false
        } else false
      } else false
    }).getOrElse(false)
  }

  def chooseThrottle(state: State): Double = {
    (for {
      race <- state.raceOpt
      currentPosition <- state.positionOpt
      currentPiece <- currentPosition.currentPiece(race.track)
      nextPiece <- currentPosition.nextPiece(race.track)
      maxAngle = state.mechanics.maxAngle
      targetG = state.mechanics.targetG
    } yield {
      currentPiece match {
        case _: Straight =>
          if (shouldBrake(state)) 0
          else 1.0
        case Bend(radius, angle, _) =>
          state.lateralGOpt.map { lateralG =>
            // if we're breaking for a corner, focus on that
            if (shouldBrake(state)) 0
            // no drama if we're at a comfy angle
            else if (currentPosition.angle.abs < maxAngle * 0.4)
              1.0
            // otherwise watch the slip angle for danger!
            else if (currentPosition.angle.abs > maxAngle * 0.9)
              (1 - (currentPosition.angle.abs / maxAngle)).clamp(0, 1.0)
            // otherwise keep an eye on the targetG
            else if (lateralG > targetG * 0.9)
              (1 - (lateralG / targetG)).clamp(0, 1.0)
            else 1.0
          }.getOrElse(1.0)
      }
    }).getOrElse(1.0)
  }

  def isItTimeForTheTurboButton(state: State): Boolean = {
    (for {
      race <- state.raceOpt
      currentPosition <- state.positionOpt
      currentPiece <- currentPosition.currentPiece(race.track)
      speed <- state.speedOpt
      turbo <- state.availableTurbo
    } yield {
      currentPiece match {
        case _: Straight =>
          state.distanceToNextBend.map { distanceToNextBend =>
            // use turbo if it would fit in current straight at current speed
            if (turbo.turboDurationTicks * 10 < distanceToNextBend) true
            else false
          }.getOrElse(true) // or if there are no bends coming up
        case _ => false
      }
    }).getOrElse(false)
  }

  implicit class RichDouble(d: Double) {
    def clamp(min: Double, max: Double): Double = {
      Math.max(Math.min(max, d), min)
    }
  }
}
