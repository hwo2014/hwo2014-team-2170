package hwo2014

import org.json4s.JsonAST.JString
import scala.annotation.tailrec


/** Command to switch on the given track piece index from the given start lane to the given end lane */
case class SwitchCommand(
  trackPieceIndex: Int,
  startLane: Int,
  endLane: Int,
  messageSent: Boolean
) {
  def positionTuple = (trackPieceIndex, startLane, endLane)

  def isLeft = endLane < startLane

  def toMessage = MsgWrapper("switchLane", if (isLeft) JString("Left") else JString("Right"))
}

case class PositionInfo(
  currentPosition: CarPosition,
  previousPosition: Option[(CarPosition, Tick)] = None
)

case class State(
  gameTickOpt: Option[Tick] = None,
  myCarOpt: Option[CarId] = None,
  positions: Map[CarId, PositionInfo] = Map.empty,
  raceOpt: Option[Race] = None,
  mechanics: Mechanics = Mechanics.initial,
  lastSwitchCommand: Option[SwitchCommand] = None,
  availableTurbo: Option[Turbo] = None,
  lastTick: Int = 0
) {
  def setLastSwitchCommandSent = copy(lastSwitchCommand = lastSwitchCommand.map(_.copy(messageSent = true)))

  lazy val positionOpt = myCarOpt.flatMap(positions.get).map(_.currentPosition)
  lazy val previousPositionOpt = myCarOpt.flatMap(positions.get).flatMap(_.previousPosition)

  lazy val speedOpt: Option[Double] =  // distance units / tick
    for {
      race <- raceOpt
      currentPosition <- positionOpt
      gameTick <- gameTickOpt
      (previousPosition, previousGameTick) <- previousPositionOpt
      distanceDelta =
        if (previousPosition.piecePosition.pieceIndex == currentPosition.piecePosition.pieceIndex)
          currentPosition.piecePosition.inPieceDistance - previousPosition.piecePosition.inPieceDistance
        else {
          // some unsafe gets here - should be ok unless track is changing mid-race!
          val previousTrackPiece = race.track.trackPieces(previousPosition.piecePosition.pieceIndex)
          val previousLane = race.track.lanes.find(lane => lane.index == previousPosition.piecePosition.lane.endLaneIndex).get
          currentPosition.piecePosition.inPieceDistance +
            (previousTrackPiece.laneLength(previousLane) - previousPosition.piecePosition.inPieceDistance)
        }
      timeDelta = gameTick - previousGameTick
      currentSpeed <-
        if (0 == timeDelta) None
        else Some(distanceDelta / timeDelta)
    } yield currentSpeed

  lazy val lateralGOpt: Option[Double] = {
    for {
      race <- raceOpt
      currentPosition <- positionOpt
      currentPiece <- currentPosition.currentPiece(race.track)
      currentLane <- currentPosition.currentLane(race.track)
      currentSpeed <- speedOpt
    } yield {
      currentPiece match {
        case _: Straight => 0
        case bend: Bend => Mechanics.calculateLateralG(bend.laneRadius(currentLane), currentSpeed)
      }
    }
  }

  lazy val onBendOpt: Option[Boolean] = {
    for {
      race <-raceOpt
      position <- positionOpt
      currentPiece <- position.currentPiece(race.track)
    } yield currentPiece.isBend
  }

  // TODO: don't take pieces past the end of the race (to ensure home straight turbo)
  lazy val upcomingPiecesOpt: Option[List[TrackPiece]] = {
    for {
      race <- raceOpt
      position <- positionOpt
      track = race.track
      pieceIndex = position.piecePosition.pieceIndex
    } yield track.trackPieces.drop(pieceIndex + 1) ++ track.trackPieces.take(pieceIndex)
  }

  lazy val nextBend: Option[Bend] =
    nextCurve.flatMap(_.headOption)

  lazy val nextCurve: Option[List[Bend]] = {
    for {
      upcomingPieces <- upcomingPiecesOpt
      nextBend <- upcomingPieces.collectFirst {
        case bend: Bend => bend
      }
      isLeftTurn = nextBend.angle < 0
    } yield {

      upcomingPieces
        .dropWhile(_.isInstanceOf[Straight])
        .takeWhile(_.isInstanceOf[Bend])
        .collect {
          case bend: Bend => bend
        }
        .takeWhile { bend => (bend.angle > 0.0) == (nextBend.angle > 0.0) }
    }
  }

  lazy val distanceToNextBend: Option[Double] = {
    for {
      race <- raceOpt
      bend <- nextBend
      upcomingPieces <- upcomingPiecesOpt
      position <- positionOpt
      currentPiece <- position.currentPiece(race.track)
      currentLane <- position.currentLane(race.track)
    } yield {
      val yetToTraverse = upcomingPieces.view.map {
        case Straight(length, _) => Some(length)
        case _ => None
      }.takeWhile(_.isDefined).flatten.sum
      val remainingInThisPiece = currentPiece.laneLength(currentLane) - position.piecePosition.inPieceDistance
      yetToTraverse + remainingInThisPiece
    }
  }
}

case class Mechanics(knownSafeG: Option[Double], knownUnsafeG: Option[Double], targetG: Double, decelerationFactor: Double, maxAngle: Double) {
  require(decelerationFactor < 1 && decelerationFactor > 0, "deceleration factor must be in range 0 < df < 1")

  def deceleration(speed: Double) = {
    // TODO deceleration linear by speed using factor
    decelerationFactor * speed
  }

  def distanceToSlow(startV: Double, endV: Double): Double = {
    require(startV > endV, "the end velocity must be less than the start")
    @tailrec
    def loop(distanceSoFar: Double, velocity: Double): Double = {
      if (velocity <= endV) distanceSoFar
      else loop(distanceSoFar + velocity, velocity * decelerationFactor)
    }
    loop(0, startV)
  }

  def goodGValue(lateralG: Option[Double]): Mechanics = {
    (for {
      currentSafe <- knownSafeG.orElse(Some(0.toDouble))
      newSafe <- lateralG if newSafe > currentSafe
    } yield copy(knownSafeG = Some(newSafe))).getOrElse(this)
  }

  def badGValue(lateralG: Option[Double]): Mechanics = {
    (for {
      currentUnsafe <- knownUnsafeG.orElse(Some(999999999.toDouble))
      newUnsafe <- lateralG if newUnsafe < currentUnsafe
    } yield copy(knownUnsafeG = Some(newUnsafe), targetG = Math.min(targetG, newUnsafe)))
      .getOrElse(copy(targetG = Math.min(targetG, lateralG.getOrElse(targetG))))
  }
}
object Mechanics {
  // use this to tune sensible start values and it'll learn from there within the race
  val initial = Mechanics(None, None,
    0.5,   // observed safe lateralG (a little conservative perhaps?)
    0.98,   // deceleration factor observed with high accuracy
    59.0
  )

  def calculateLateralG(radius: Int, speed: Double): Double = Math.pow(speed, 2) / radius
  def calculateTargetSpeed(radius: Int, targetLateralG: Double): Double = Math.sqrt(radius * targetLateralG)
}
