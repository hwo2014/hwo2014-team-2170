package hwo2014

case class CarId(
  name: String,
  color: String
)

case class Dimensions(
  length: Double,
  width: Double,
  guideFlagPosition: Double
)

case class Car(
  id: CarId,
  dimensions: Dimensions
)

case class RaceSession(
  laps: Option[Int],
  durationMs: Option[Int],
  maxLapTimeMs: Option[Int],
  quickRace: Option[Boolean]
)

/** Nasty, but polymorphism under JsonAST expects a type hint encoded in the JSON. There might be a way round that
  * but I've not had chance to look (or change JSON library, which may be the better solution).
  */
case class _TrackPiece(
  radius: Option[Int],
  angle: Option[Double],
  length: Option[Double],
  switch: Option[Boolean]
) {
  def toTrackPiece: TrackPiece = (length, switch, radius, angle) match {
    case (Some(l), _, _, _) => Straight(l, switch.getOrElse(false))
    case (_, _, Some(r), Some(a)) => Bend(r, a, switch.getOrElse(false))
    case _ => throw new RuntimeException("Track piece should have length or angle and radius")
  }
}

trait TrackPiece {
  def laneLength(lane: Lane): Double
  val switch: Boolean
  val isBend: Boolean

  /** Length of path from start lane to end lane. Option as not all pieces are switches */
  def pathLength(start: Lane, end: Lane): Option[Double]
}
case class Straight(length: Double, switch: Boolean) extends TrackPiece {
  override val isBend: Boolean = false
  override def laneLength(lane: Lane) = length
  def switchLength(lane1: Lane, lane2: Lane) =
    Math.sqrt(Math.pow(length, 2) + Math.pow((lane1.distanceFromCenter - lane2.distanceFromCenter).abs, 2))

  /** Length of path from start lane to end lane. Option as not all pieces are switches */
  override def pathLength(start: Lane, end: Lane): Option[Double] = if (start == end)
      Some(laneLength(start))
    else if (switch)
      Some(switchLength(start, end))
    else
      None
}
case class Bend(radius: Int, angle: Double, switch: Boolean = false) extends TrackPiece {
  override val isBend: Boolean = true
  val radians = (angle / 180) * Math.PI
  override def laneLength(lane: Lane) = {
    if (angle < 0) {
      // left turn, +ve distanceFromCentre is longer
      (radius + lane.distanceFromCenter) * radians
    } else {
      // right turn, +ve distanceFromCentre is shorter
      (radius - lane.distanceFromCenter) * radians
    }
  }.abs

  def laneRadius(lane: Lane) = {
    if (angle < 0) {
      // left turn, +ve distanceFromCentre is longer
      radius + lane.distanceFromCenter
    } else {
      // right turn, +ve distanceFromCentre is shorter
      radius - lane.distanceFromCenter
    }

  }

  /** Length of path from start lane to end lane. Option as not all pieces are switches */
  override def pathLength(start: Lane, end: Lane): Option[Double] =
    if (start == end) {
      Some(laneLength(start))
    } else if (switch) {
      val shortLane = Math.min(laneLength(start), laneLength(end))
      val approxLength = shortLane + ((angle / 90) * (laneRadius(start) - laneRadius(end))).abs
      Some(approxLength)
    } else {
      None
    }
}

case class Lane(
  distanceFromCenter: Int,
  index: Int
)

case class Position(
  x: Double,
  y: Double
)

case class StartingPoint(
  position: Position,
  angle: Double
)

case class Track(
  id: String,
  name: String,
  pieces: List[_TrackPiece],
  lanes: List[Lane],
  startingPoint: StartingPoint
) {
  lazy val trackPieces: List[TrackPiece] = pieces.map(_.toTrackPiece)
  def trackPeiceByIndex(index: Int): Option[TrackPiece] = trackPieces.lift(index)
  def laneByIndex(index: Int): Option[Lane] = lanes.lift(index)
}

case class Race(
  track: Track,
  cars: List[Car],
  raceSession: RaceSession
)

case class GameInit(
  race: Race
)

case class LanePosition(
  startLaneIndex: Int,
  endLaneIndex: Int
)

case class PiecePosition(
  pieceIndex: Int,
  inPieceDistance: Double,
  lane: LanePosition,
  lap: Int
)

case class CarPosition(
  id: CarId,
  angle: Double,
  piecePosition: PiecePosition
) {
  def currentPiece(track: Track): Option[TrackPiece] = track.trackPeiceByIndex(piecePosition.pieceIndex)
  def nextPiece(track: Track): Option[TrackPiece] = track.trackPeiceByIndex((piecePosition.pieceIndex + 1) % track.trackPieces.size)
  def currentLane(track: Track): Option[Lane] = track.laneByIndex(piecePosition.lane.endLaneIndex)
}

/** Looks like either all or none of these are present - not sure why none would be, unless the car did not complete
  * a single lap
  */
case class ResultStats(
  laps: Option[Int],
  ticks: Option[Int],
  millis: Option[Int]
)

case class Result(
  car: CarId,
  result: ResultStats
)

case class GameEnd(
  results: List[Result],
  bestLaps: List[Result]
)

case class LapTiming(
  lap: Int,
  ticks: Int,
  millis: Int
)

case class RaceTiming(
  laps: Int,
  ticks: Int,
  millis: Int
)

case class Ranking(
  overall: Int,
  fastestLap: Int
)

case class LapFinished(
  car: CarId,
  lapTime: LapTiming,
  raceTime: RaceTiming,
  ranking: Ranking
)

case class Disqualified(
  car: Car,
  reason: String
)

case class Turbo(turboDurationMilliseconds: Int, turboDurationTicks: Int, turboFactor: Double)
