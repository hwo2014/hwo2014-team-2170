import org.json4s.DefaultFormats

package object hwo2014 {
  implicit val formats = new DefaultFormats {

  }

  type Tick = Int
}
