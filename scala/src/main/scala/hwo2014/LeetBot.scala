package hwo2014

import org.json4s._
import org.json4s.DefaultFormats
import java.net.Socket
import java.io.{BufferedReader, InputStreamReader, OutputStreamWriter, PrintWriter}
import org.json4s.native.Serialization
import scala.annotation.tailrec
import hwo2014.PathFinding.TrackNode
import SwitchMessageLogic.RichState

object LeetBot extends App {
  args.toList match {
    case hostName :: port :: botName :: botKey :: trackName :: _ =>
      new LeetBot(hostName, Integer.parseInt(port), botName, botKey, Some(trackName))
    case hostName :: port :: botName :: botKey :: _ =>
      new LeetBot(hostName, Integer.parseInt(port), botName, botKey)
    case _ => println("args missing")
  }
}

class LeetBot(host: String, port: Int, botName: String, botKey: String, trackNameOpt: Option[String] = None) {

  val socket = new Socket(host, port)
  val writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream, "UTF8"))
  val reader = new BufferedReader(new InputStreamReader(socket.getInputStream, "UTF8"))
  trackNameOpt match {
    case None => send(MsgWrapper("join", Join(botName, botKey)))
    case Some(trackName) => send(MsgWrapper("joinRace", JoinRace(Join(botName, botKey), trackName, 1)))
  }
  val initialState = State()
  play(initialState)

  @tailrec
  private def play(state: State) {
    val line = reader.readLine()

    if (line != null) {
      val newState: State = Serialization.read[MsgWrapper](line) match {
        case MsgWrapper("yourCar", data, _) =>
          val myCar = data.extract[CarId]
          println(s"Got my car $myCar")
          state.copy(myCarOpt = Some(myCar))

        case MsgWrapper("gameInit", data, _) =>
          val gameInit = data.extract[GameInit]
          println(s"Got game init $gameInit")
          state.copy(raceOpt = Some(gameInit.race))

        case MsgWrapper("carPositions", data, Some(gameTick)) =>
          val carPositions = data.extract[List[CarPosition]]
          val updatedState = Logic.updateStateFromPositions(state.withNextSwitchCommand, carPositions, gameTick)
          Logic.updateGoodLateralG(updatedState)

        case MsgWrapper("gameEnd", data, _) =>
          val gameEnd = data.extract[GameEnd]
          println("End of game. Results:")
          gameEnd.results foreach { result =>
            println(s"${result.car.name} ${result.result.laps.getOrElse(0)} laps in " +
              s"${result.result.millis.getOrElse(0)} millis ${result.result.ticks.getOrElse(0)} ticks")
          }
          state

        case MsgWrapper("crash", data, _) =>
          // if it's us, update lateral G and empty previous position
          val carId = data.extract[CarId]
          println(s"${carId.name} crashed!")
          state.myCarOpt.find(_ == carId).foreach { _ =>
            for {
              position <- state.positionOpt
              speed <- state.speedOpt
              lateralG <- state.lateralGOpt
            } println(s"crashed at angle: ${position.angle}, speed: $speed, lateralG: $lateralG")
          }
          Logic.updateStateFromCrash(carId, state)

        case MsgWrapper("spawn", data, _) =>
          val car = data.extract[CarId]
          println(s"${car.name} respawns")
          state

        case MsgWrapper("lapFinished", data, _) =>
          val lapFinished = data.extract[LapFinished]
          println(s"Lap finished: $lapFinished")
          state

        case MsgWrapper("dnf", data, _) =>
          val disqualified = data.extract[Disqualified]
          println(s"Disqualified! $disqualified")
          state

        case MsgWrapper("finish", data, _) =>
          val car = data.extract[CarId]
          println(s"Finished: $car")
          state

        case MsgWrapper("turboAvailable", data, _) =>
          val turbo = data.extract[Turbo]
          println(s"turbo available: $turbo")
          state.copy(availableTurbo = Some(turbo))

        case MsgWrapper(msgType, _, _) =>
          println("Received unexpected message: " + msgType)
          state
      }

      Serialization.read[MsgWrapper](line) match {
        case MsgWrapper(_, _, Some(gameTick)) =>  // should respond to all tick messages from server
          // potential messages
          val switchMessage = SwitchMessageLogic.switchMessage(state)
          val turboButtonMessage =
            if (Logic.isItTimeForTheTurboButton(state)) Some(MsgWrapper("turbo", "For open journalism!"))
            else None
          val throttle = Logic.chooseThrottle(newState)

          val (message, finalState) =
            turboButtonMessage.map((_, newState.copy(availableTurbo = None)))
              .orElse(switchMessage.map((_, newState.setLastSwitchCommandSent)))
              .getOrElse((MsgWrapper("throttle", throttle), newState))

          println(s"motionInfo :: speed: ${state.speedOpt}, lateralG: ${state.lateralGOpt}, throttle: $throttle, angle: ${state.positionOpt.map(_.angle)}")

          send(message)
          play(finalState)
        case _ =>
          // no tick so no need to respond
          println("non-tick message, staying quiet")
          play(newState)
      }
    }
  }

  def send(msg: MsgWrapper) {
    val body = Serialization.write(msg)
    writer.println(body)
    writer.flush()
  }
}
