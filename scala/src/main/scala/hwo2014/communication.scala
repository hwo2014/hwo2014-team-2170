package hwo2014

import org.json4s._

case class Join(name: String, key: String)

case class JoinRace(botId: Join, trackName: String, carCount: Int)

case class MsgWrapper(msgType: String, data: JValue, gameTick: Option[Int])
object MsgWrapper {
  implicit val formats = new DefaultFormats{}

  def apply(msgType: String, data: Any, gameTick: Option[Tick]): MsgWrapper = {
    MsgWrapper(msgType, Extraction.decompose(data), gameTick)
  }

  def apply(msgType: String, data: Any): MsgWrapper = {
    MsgWrapper(msgType, Extraction.decompose(data), None)
  }
}
