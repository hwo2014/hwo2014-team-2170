package hwo2014

import hwo2014.PathFinding.TrackNode

object SwitchMessageLogic {
  implicit class RichState(state: State) {
    def withNextSwitchCommand = {
      (for {
      /** The if here prevents this from stomping the 'messageSent' switch, which prevents us repeatedly messaging
        * the server about the switch
        */
        switch <- nextSwitch(state) if !state.lastSwitchCommand.exists(_.positionTuple == switch.positionTuple)
      } yield state.copy(lastSwitchCommand = Some(switch))) getOrElse state
    }
  }

  implicit class RichPath(path: Seq[PathFinding.Node]) {
    def toSwitchCommand = path.sliding(2) collectFirst {
      case TrackNode(track1, lane1, _) :: TrackNode(track2, lane2, _) :: Nil if lane1 != lane2 => SwitchCommand(
        track1,
        lane1,
        lane2,
        false
      )
    }
  }

  /** Next switch command if applicable */
  def nextSwitch(state: State) =
    for {
      race <- state.raceOpt
      myPosition <- state.positionOpt
      path <- PathFinding.findPath2(
        myPosition.piecePosition,
        race.track.lanes,
        race.track.trackPieces
      )
      switchCommand <- path.toSwitchCommand
    } yield switchCommand

  /** If, given the state, we now need to send a switch message, that message. Otherwise None. */
  def switchMessage(state: State) = {
    for {
      race <- state.raceOpt
      myPosition <- state.positionOpt
      lastSwitchCommand <- state.lastSwitchCommand if !lastSwitchCommand.messageSent &&
      (lastSwitchCommand.trackPieceIndex == (myPosition.piecePosition.pieceIndex + 1) %
        race.track.pieces.length)
    } yield lastSwitchCommand.toMessage
  }
}
