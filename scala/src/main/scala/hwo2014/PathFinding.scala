package hwo2014

object PathFinding {
  case class Neighbour(node: Node, distance: Double)

  sealed trait Node
  case class TrackNode(trackIndex: Int, lane: Int, neighbours: Set[Neighbour]) extends Node
  case object Finish extends Node

  def prettyPrint(node: Node) = node match {
    case TrackNode(track, lane, _) => s"TrackNode(trackPiece=$track, lane=$lane)"
    case Finish => "Finish"
  }

  def makeGraph(piecePosition: PiecePosition, lanes: List[Lane], pieces: List[TrackPiece]) = {
    require(pieces.length > 0, "Must supply at least 1 track piece to make the graph")

    val indexOffset = piecePosition.pieceIndex
    val numberOfLanes = lanes.length

    val piecesWithIndex = pieces.zipWithIndex
    val remainingInLap = piecesWithIndex.drop(indexOffset) ++ piecesWithIndex.take(indexOffset)

    val nextPiece = remainingInLap.drop(1).foldRight[List[Node]](List.fill(numberOfLanes)(Finish)) {
      case ((trackPiece, trackPieceIndex), nodes) =>
        lanes.zipWithIndex map {
          case (lane, laneIndex) =>
            val straightPath = Neighbour(nodes(laneIndex), trackPiece.laneLength(lane))

            def switchForIndex(i: Int) = for {
              laneTo <- lanes.lift(i)
              node <- nodes.lift(i)
              path <- trackPiece.pathLength(lane, laneTo)
            } yield Neighbour(node, path)

            val switchPaths = List(
              switchForIndex(laneIndex - 1),
              switchForIndex(laneIndex + 1)
            ).flatten

          TrackNode(trackPieceIndex, laneIndex, (straightPath :: switchPaths).toSet)
        }
    }

    /** As we're already travelling on the initial piece, there is no way to switch lanes */
    remainingInLap.head match { case (trackPiece, trackPieceIndex) =>
      val pathLength = for {
        startLane <- lanes.lift(piecePosition.lane.startLaneIndex)
        endLane <- lanes.lift(piecePosition.lane.endLaneIndex)
        length <- trackPiece.pathLength(startLane, endLane)
      } yield length

      TrackNode(
        trackPieceIndex,
        piecePosition.lane.startLaneIndex,
        Set(Neighbour(nextPiece(piecePosition.lane.endLaneIndex),
          pathLength.getOrElse(throw new RuntimeException("Start lane or end lane is incorrect for first piece")) -
            piecePosition.inPieceDistance
        ))
      )
    }
  }

  def reconstructPath(cameFrom: Map[Node, Node], node: Node) = {
    def iter(node: Node): List[Node] =
      cameFrom.get(node).map(parent => node :: iter(parent)).getOrElse(List(node))

    iter(node).reverse
  }

  /** Heuristic for the track piece back to itself based on the minimum length of each possible track piece.
    *
    * i.e., for a straight path this is constant. For a curved piece of track it will calculate the cost of the inner
    * lane.
    *
    * @param pieces The track pieces
    * @param trackIndex The index of the piece to calculate the cost
    * @return The predicted cost
    */
  def minimumTraversalHeuristic(pieces: List[TrackPiece], lanes: List[Lane])(trackIndex: Int): Double = {
    val path = pieces.drop(trackIndex) ++ pieces.take(trackIndex)

    path map {
      case straight: Straight => straight.length

      case bend: Bend => lanes.map(bend.laneLength).min
    } reduce { _ + _ }
  }

  /** A* search */
  def findPath(startNode: Node, heuristic: Node => Double) = {
    /** TODO, replace the open set with a priority queue based on the gScore, for efficiency reasons */
    @annotation.tailrec
    def search(
        closed: Set[Node],
        open: Set[Node],
        cameFrom: Map[Node, Node],
        gScore: Map[Node, Double]
    ): Option[List[Node]] = {
      def fScore(node: Node) = gScore(node) + heuristic(node)

      if (open.isEmpty) {
        /** No path to end! */
        None
      } else {
        val current = open.toSeq.minBy(fScore)

        current match {
          case Finish => Some(reconstructPath(cameFrom, current))

          case TrackNode(index, lane, neighbours) =>
            val (newCameFrom, newGScore, newOpen) =
              neighbours.filterNot(closed contains _.node).foldLeft(cameFrom, gScore, open) {
                case ((cameFromAcc, gScoreAcc, openAcc), Neighbour(node, distance)) =>
                  val tentativeGScore = gScoreAcc.getOrElse(current, 0d) + distance

                  if (!(open contains node) || tentativeGScore < gScoreAcc(node)) {
                    (cameFromAcc + (node -> current), gScoreAcc + (node -> tentativeGScore), openAcc + node)
                  } else {
                    (cameFromAcc, gScoreAcc, openAcc)
                  }
            }

            search(closed + current, newOpen - current, newCameFrom, newGScore)
        }
      }
    }

    search(Set.empty, Set(startNode), Map.empty, Map(startNode -> 0))
  }

  def findPath2(piecePosition: PiecePosition, lanes: List[Lane], pieces: List[TrackPiece]) = {
    val startNode = makeGraph(piecePosition, lanes, pieces)

    val heuristic = (node: Node) => node match {
      case Finish => 0
      case node: TrackNode => minimumTraversalHeuristic(pieces, lanes)(node.trackIndex)
    }

    findPath(startNode, heuristic)
  }
}
